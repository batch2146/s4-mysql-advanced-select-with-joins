USE music_db;

-- find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- join the 'album' and 'songs' tables.
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- join the 'artists' and 'albums' tables (find all albums that has a letter a in its name)
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- sort the albums in Z-A (show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- join the 'albums' and 'songs' tables (sort albums from z-a)
SELECT * FROM songs
    JOIN albums ON albums.id = songs.album_id ORDER BY albums.album_title DESC;